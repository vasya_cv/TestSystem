<?php
require_once 'app/models/Test.php';
require_once 'app/models/Question.php';
require_once 'app/models/QuestionUser.php';
class ResultController extends Controller
{
    function execute()
    {

        if(!empty($_POST)){

            $test_id = $_POST['test'];

        }

        if(!empty($_GET['id'])){

            $test_id = $_GET['id'];

        }

            $question = new Question();

            $questionUser = new QuestionUser();

            $test = new Test();

            $user_id = $_SESSION['user_id'];

            foreach ($_POST as $k=>$item){

                $question_id = $k;

                $answer = $item;

                $d_user = $questionUser->set_data($user_id, $question_id, $answer);
            }

            $bal = $question->check_answer($user_id, $test_id);

            $mark = $test->get_mark($test_id);

            $data = array();

            $percent = round($bal[0]['bal']/$mark[0]['mark'] * 100, 0);

            $data['bal'] = (int)$bal[0]['bal'];
            $data['mark'] = $mark[0]['mark'];
            $data['percent'] = $percent;

            $user_answer = $questionUser->get_data($test_id,$user_id);

            $data_table = array();

            $i=0;
            foreach($user_answer as $val){

                $answ = $val['answer'];
                $cor = $val['correct'];

                $data_table[$i]= ['question' => $val['title'],
                    'user_answer' => $val['variant'.$answ],
                    'correct_answer' => $val['variant'.$cor]];

                $i++;
            }

            $this->view->generate('result_view.php','template_view.php',$data, $data_table);
    }
}