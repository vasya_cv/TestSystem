<?php
require_once 'app/models/User.php';
class IndexController extends Controller
{
    function execute()
    {
        $user = new User();

         if(!empty($_POST)){

             if(!empty($_POST['login']) && !empty($_POST['password'])){

                 $login = Validation::validStr($_POST['login']);
                 $password = Validation::validStr($_POST['password']);

                 $singIn = $user->login($login,$password);

                   if(!empty($singIn)){
                      foreach ($singIn as $item){
                          $_SESSION['user_id'] = $item['id'];
                          $_SESSION['user_login'] = $item['login'];
                          $_SESSION['role_id'] = $item['role_id'];
                      }

                      if($item['role_id'] == 1){
                          parent::redirect('admin');
                      }elseif($item['role_id'] == 2){       //to do for user_id
                          parent::redirect('site');
                      }

                   }else{
                       $_SESSION['error_log'] = "Перевірте логін та пароль";
                       parent::redirect('');
                   }

             }else{
                 $_SESSION['error_log'] = "Введіть логін та пароль";
                 parent::redirect('');
             }

        }else{
             $this->view->generate('main_view.php','template_view.php');
             session_destroy();
        }

    }

}

