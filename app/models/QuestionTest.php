<?php

class QuestionTest extends Model
{
    public function set_dat($questionId, $testId)
    {
        if (!empty($questionId) && !empty($testId))
        {

            try {
                $sql = "INSERT INTO question_test (question_id, test_id) VALUES (
                :question_id,
                :test_id)";

                $addQuestionTest = $this->db->prepare($sql);

                $addQuestionTest->bindParam(':question_id', $questionId, PDO::PARAM_STR);
                $addQuestionTest->bindParam(':test_id', $testId, PDO::PARAM_STR);

                $addQuestionTest->execute();

                return true;
            }
            catch( PDOException $e ) {

                return $e->getMessage();

            }

        } else {
            return false;
        }
    }

    public function get_data($id)
    {
        try {
            $res = $this->db->prepare('SELECT test.id as t_id, test.title as t_title, 	test.description, question.id as q_id, question.title as q_title,
                              question.variant1, question.variant2, question.variant3, question.variant4, question.correct, question.mark 
                              FROM question_test
                              INNER JOIN question ON question_test.question_id = question.id
                              INNER JOIN test ON question_test.test_id = test.id
                              WHERE question_test.test_id = ?');

            if (!$res) {
                return FALSE;
            }

            $res->execute([$id]);

            $data = $res->fetchAll(PDO::FETCH_ASSOC);

            return $data;
        }
        catch( PDOException $e ) {

            return $e->getMessage();
        }
    }


}