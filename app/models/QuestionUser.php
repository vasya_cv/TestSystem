<?php
require_once 'app/models/Test.php';
class QuestionUser extends Model
{
    private $user_id;
    private $question_id;
    private $answer;

    public function set_data($user_id, $question_id, $answer){
        if (!empty($user_id))
        {
            try {
                $sql = "INSERT INTO question_user (user_id, question_id, answer) VALUES (:user_id, :question_id, :answer) ON DUPLICATE KEY UPDATE answer = :answer";

                $addQuestion = $this->db->prepare($sql);

                $addQuestion->bindParam(':user_id', $user_id, PDO::PARAM_STR);
                $addQuestion->bindParam(':question_id', $question_id, PDO::PARAM_STR);
                $addQuestion->bindParam(':answer', $answer, PDO::PARAM_STR);

                $addQuestion->execute();

                return true;
          }
          catch( PDOException $e ) {

               return $e->getMessage();

          }

        } else {
            return false;
        }
    }

    public function get_data($id, $user_id){

        try {
            $res = $this->db->prepare('SELECT * FROM question_user
                        left join question on question_user.question_id = question.id
                        left join question_test on question_user.question_id = question_test.question_id
                        WHERE question_test.test_id = ? AND question_user.user_id = ?');

            if (!$res) {
                return FALSE;
            }

            $res->execute([$id, $user_id]);

            $data = $res->fetchAll(PDO::FETCH_ASSOC);

            return $data;
        }
        catch( PDOException $e ) {

            return $e->getMessage();
        }
    }

    public function get_tests($user_id){

        try {
            $res = $this->db->prepare('SELECT DISTINCT (test.id), test.title FROM question_user
                        LEFT JOIN question ON question_user.question_id = question.id
                        LEFT JOIN question_test ON question_user.question_id = question_test.question_id
                        LEFT JOIN test ON question_test.test_id = test.id
                        WHERE question_user.user_id = ?');

            if (!$res) {
                return FALSE;
            }

            $res->execute([$user_id]);

            $data = $res->fetchAll(PDO::FETCH_ASSOC);

            return $data;
        }
        catch( PDOException $e ) {

            return $e->getMessage();
        }
    }
}