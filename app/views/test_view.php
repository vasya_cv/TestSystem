<div class="container">
    <div class="row text-center test-head">
        <?php foreach ($data as $k=>$item):?>
            <?php if($k == 0):?>
                <h1>
                    <?php echo $item['t_title']?>
                </h1>
                <p><?php echo $item['description']?></p>
            <?php endif;?>
        <?php endforeach;?>
    </div>


    <div class="row test-body">
        <form id="TestForm" class="form-horizontal" action="result" method="POST">
        <?php
        $count = count($data);
        $i=1;
        foreach ($data as $k=>$item):?>
            <?php if($i==1):?>
            <div id='question<?php echo $i;?>' class='cont'>
                <h2><?php echo $i.'. '.$item['q_title'];?></h2>
                    <input type="hidden" name="test" value="<?php echo $item['t_id'];?>">
                <div class="radio">
                    <label>
                        <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="1" required><?php echo htmlentities($item['variant1']);?>
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="2"><?php echo htmlentities($item['variant2']);?>
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="3"><?php echo htmlentities($item['variant3']);?>
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="4"><?php echo htmlentities($item['variant4']);?>
                    </label>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <button id='next<?php echo $i;?>' class='next btn btn-success' type='button'>Наступне</button>
                    </div>
                </div>

            </div>
                <?php elseif ($i<1 || $i<$count):?>
                <div id='question<?php echo $i;?>' class='cont'>
                    <h2><?php echo $i.'. '.$item['q_title'];?></h2>
                    <div class="radio">
                        <label>
                            <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="1" required><?php echo htmlentities($item['variant1']);?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="2"><?php echo htmlentities($item['variant2']);?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="3"><?php echo htmlentities($item['variant3']);?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="4"><?php echo htmlentities($item['variant4']);?>
                        </label>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <button id='pre<?php echo $i;?>' class='previous btn btn-success' type='button'>Попереднє</button>
                            <button id='next<?php echo $i;?>' class='next btn btn-success' type='button' >Наступне</button>
                        </div>
                    </div>

                </div>
            <?php elseif ($i==$count):?>
                <div id='question<?php echo $i;?>' class='cont'>
                    <h2><?php echo $i.'. '.$item['q_title'];?></h2>
                    <div class="radio">
                        <label>
                            <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="1" required><?php echo htmlentities($item['variant1']);?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="2"><?php echo htmlentities($item['variant2']);?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="3"><?php echo htmlentities($item['variant3']);?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" id="radio1_<?php echo $item['q_id'];?>" name="<?php echo $item['q_id'];?>" value="4"><?php echo htmlentities($item['variant4']);?>
                        </label>
                    </div>

                    <div class="form-group">
                        <div class=" col-sm-12">
                            <button id='pre<?php echo $i;?>' class='previous btn btn-success' type='button'>Попереднє</button>
                            <button id='submit' class='next btn btn-warning' type='submit'>Завершити</button>
                        </div>
                    </div>
                </div>
            <?php endif;?>
        <?php $i++; endforeach;?>
        </form>

    </div>

</div>



