<!DOCTYPE html>
<html>
<head>
	<title>Система тестування</title>
	<meta charset="UTF-8">
	<link href="template/css/style.css" rel="stylesheet">
	<link href="template/css/bootstrap.min.css" rel="stylesheet">


</head>
<body>

<div class="container">

	<header>
        <div class="container">
            <?php if( !empty($_SESSION) && !empty($_SESSION['error'])):?>
                <div id="alertdiv" class="alert alert-danger">
                    <ul>
                        <?php echo $_SESSION['error'];?>
                    </ul>
                </div>
            <?php endif; ?>

            <?php if( !empty($_SESSION) && !empty($_SESSION['status'])):?>
                <div id="successdiv" class="alert alert-success">
                    <?php echo $_SESSION['status'];?>
                </div>
            <?php endif;?>
        </div>
	</header>

	<article>
		<?php include 'app/views/'.$content_view; ?>
	</article>

	<footer>
        <div class="container text-center">

        </div>
	</footer>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="template/js/bootstrap.min.js"></script>
<script src="template/js/global.js"></script>

</body>
</html>
