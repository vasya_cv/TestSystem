<div class="container">
    <div class="row text-center">
        <h3>Додати тест</h3>
        <form class="form-horizontal" action="admin" method="POST">
            <div class="form-group">
                <label for="inputTitle" class="col-md-2 control-label">Назва тесту</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="inputTitle" placeholder="Назва тесту" name="testTitle">
                </div>
            </div>
            <div class="form-group">
                <label for="inputDescription" class="col-md-2 control-label">Опис тесту</label>
                <div class="col-md-10">
                    <textarea class="form-control" rows="2" id="inputDescription" name="testDescription"></textarea>
                </div>
            </div>
                <div class="input_fields_wrap">

                    <div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Питання №1</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" placeholder="Питання" name="question[1][]" required>
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Варіант відповіді №1</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" placeholder="Відповідь" name="variant[1][]" required>
                                </div>
                                <div class="col-md-1">
                                    <label class="radio-inline">
                                        <input type="radio" name="correct[1][]" value="1" required>
                                    </label>
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Варіант відповіді №2</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="Відповідь" name="variant[1][]" required>
                            </div>
                            <div class="col-md-1">
                                <label class="radio-inline">
                                    <input type="radio" name="correct[1][]" value="2">
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Варіант відповіді №3</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="Відповідь" name="variant[1][]" required>
                            </div>
                            <div class="col-md-1">
                                <label class="radio-inline">
                                    <input type="radio" name="correct[1][]" value="3">
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Варіант відповіді №4</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="Відповідь" name="variant[1][]" required>
                            </div>
                            <div class="col-md-1">
                                <label class="radio-inline">
                                    <input type="radio" name="correct[1][]" value="4">
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Оцінка</label>
                            <div class="col-md-10">
                                <input type="number" class="form-control" placeholder="Оцінка" name="mark[1][]" required>
                            </div>
                        </div>
                </div>

            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <button class="btn btn-primary add_field_button">Додати ще питання</button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Записати тест</button>
                </div>
            </div>
        </form>
    </div>
</div>


