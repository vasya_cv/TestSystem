<div class="container archive-list">
    <div class="row pull-right">
        <h4><a href="site" class="btn bg-primary">Перелік тестів</a></h4>
    </div>

    <div class="row text-center">
        <h3>Архів пройдених тестів</h3>
    </div>
    <div class="row">
        <div class="list-group">
            <?php foreach($data as $item):?>
            <a href="result?id=<?php echo $item['id'];?>" class="list-group-item ">
                <?php echo $item['title'];?>
            </a>
            <?php endforeach;?>
        </div>
    </div>
</div>